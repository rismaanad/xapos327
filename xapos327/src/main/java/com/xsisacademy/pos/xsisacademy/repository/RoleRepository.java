package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
	List<Role> findByIsDelete(Boolean isDelete);
	
		@Query(value = " select * from tbl_role where lower(role_name) like lower(concat('%',?1,'%')) and is_delete = ?2 order by role_name asc", nativeQuery = true)
		Page<Role> findByIsDelete(String keyword, Boolean isDelete, Pageable page);

		@Query(value = " select * from tbl_role where lower(role_name) like lower(concat('%',?1,'%')) and is_delete = ?2 order by role_name desc", nativeQuery = true)
		Page<Role> findByIsDeleteDESC(String keyword, Boolean isDelete, Pageable page);

}
