package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

	@Query(value = "select * from category where is_active = true order by category_name", nativeQuery = true)//pakai Native query
	//@Query(value = "SELECT c FROM Category c where c.isActive = true")//Pakai Java Class
	List<Category> findByCategories();
	
	//PAGING//
	@Query(value = "select * from category where lower(category_name) like lower(concat ('%',?1,'%')) and is_active = ?2 order by category_name asc ", nativeQuery = true)
	Page<Category> findByIsActive(String keyword,Boolean isActive, Pageable page );
	
	@Query(value = "select * from category where lower(category_name) like lower(concat ('%',?1,'%')) and is_active = ?2 order by category_name desc ", nativeQuery = true)
	Page<Category> findByIsActiveDESC(String keyword,Boolean isActive ,Pageable page);
		
	@Query(value = "SELECT * FROM category WHERE is_active = true AND category_name =?1 LIMIT 1", nativeQuery = true)
    Category findByIdName(String categoryName);

    @Query(value = "SELECT * FROM category WHERE is_active = true AND category_name =?1 AND id != ?2 LIMIT 1", nativeQuery = true)
    Category findByIdNameForEdit(String categoryName, Long id);
}
