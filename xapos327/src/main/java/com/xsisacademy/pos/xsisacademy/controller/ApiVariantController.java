package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.variant;
import com.xsisacademy.pos.xsisacademy.repository.variantRepository;

@RestController
@RequestMapping("/api/")
public class ApiVariantController {
	
	@Autowired
	private variantRepository VariantRepository;

	@GetMapping("variant")
	public ResponseEntity<List<variant>>getAllVariant(){
		try {
			List<variant> ListVariant = this.VariantRepository.findByIsActive(true);
			return new ResponseEntity<>(ListVariant,HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	@PostMapping("variant/add")
	public ResponseEntity<Object> saveVariant(@RequestBody variant Variant){
		Variant.createBy ="admin1";
		Variant.createDate = new Date();
		
		variant VariantData = this.VariantRepository.save(Variant);
		
		if(VariantData.equals(Variant)) {
			return new ResponseEntity<>("Save Data Success",HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Save Failed",HttpStatus.BAD_REQUEST);
		}
	}
	@GetMapping("variant/{id}")
	public ResponseEntity<Object> getVariantById(@PathVariable Long id){
		try {
			Optional<variant> Variant = this.VariantRepository.findById(id);
			return new ResponseEntity<>(Variant, HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	@PutMapping("variant/edit/{id}")
	public ResponseEntity<Object> editVariant(@PathVariable("id") Long id, @RequestBody variant Variant){
		Optional<variant> VariantData = this.VariantRepository.findById(id);
		
		if(VariantData.isPresent()) {
			Variant.id = id;
			Variant.modifyBy = "admin1";
			Variant.modifyDate = new Date();
			Variant.createBy = VariantData.get().createBy;
			Variant.createDate = VariantData.get().createDate;
			
			this.VariantRepository.save(Variant);
			return new ResponseEntity<>("Update Data Success", HttpStatus.OK);
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
	@PutMapping("variant/delete/{id}")
	public ResponseEntity<Object> deleteVariant(@PathVariable("id") Long id){
		Optional<variant> VariantData = this.VariantRepository.findById(id);
		
		if(VariantData.isPresent()) {
			variant Variant = new variant();
			Variant.id = id;
			Variant.isActive = false;
			Variant.modifyBy = "admin1";
			Variant.modifyDate = new Date();
			Variant.createBy = VariantData.get().createBy;
			Variant.createDate = VariantData.get().createDate;
			Variant.categoryId = VariantData.get().categoryId;
			Variant.variantInitial = VariantData.get().variantInitial;
			Variant.variantName = VariantData.get().variantName;
			
			this.VariantRepository.save(Variant);
			return new ResponseEntity<>("Delete Data Success", HttpStatus.OK);
		}
		else {
			return ResponseEntity.notFound().build();
		}
	}
} 
