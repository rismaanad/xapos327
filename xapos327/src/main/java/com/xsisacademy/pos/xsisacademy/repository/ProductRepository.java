package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsisacademy.pos.xsisacademy.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {
	//menggunakan JpaRepository
	List<Product> findByIsActive(Boolean isActive);
	
	//mengggunakan 2 parameter
	//@Query(value = "select p from product p where p.isActive = ?1 and p.productInitial = ?2 order by p.product")
	//List<Product> findByIsActiveAndInitial(Boolean isActive, String productInitial);
}
