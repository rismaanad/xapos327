package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.variant;

public interface variantRepository extends JpaRepository<variant, Long> {

	List<variant> findByIsActive(Boolean isActive);
	
	@Query(value ="select v from variant v where v.isActive = true and v.categoryId = ?1")
	List<variant> findByCategoryId(Long categoryId);
}
