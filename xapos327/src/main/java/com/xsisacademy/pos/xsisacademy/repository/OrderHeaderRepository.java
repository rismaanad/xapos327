package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.OrderHeader;

public interface OrderHeaderRepository extends JpaRepository<OrderHeader, Long> {

	@Query("select max(o.id) from OrderHeader o") //JAVA CLASS
	public Long findByMaxId();
	
	@Query(value="select * from order_header where amount != 0 order by modify_date DESC", nativeQuery=true)
	List<OrderHeader> findAllOrderHeaderCheckout();
	
}
